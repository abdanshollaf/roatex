<?php
return [
    'header' => [
        'data1' => 'Beranda',
        'data2' => 'Tentang',
        'data3' => 'Solusi',
        'data4' => 'Teknologi',
        'data5' => 'Berita',
        'data6' => 'Karir',
    ],
    'footer1' => [
        'title' => 'PERUSAHAAN',
        'desc1' => 'Tentang',
        'desc2' => 'Pekerjaan Kami',
        'desc3' => 'Klien',
        'desc4' => 'Blog Kami',
        'desc5' => 'Hubungi Kami'
    ],
    'footer2' => [
        'title' => 'Layanan',
        'desc1' => 'Akun MLFF',
        'desc2' => 'Dapatkan Izin',
        'desc3' => 'Pasang Label',
        'desc4' => 'Bayar Tol',
        'desc5' => 'Temukan Aplikasi Cantas'
    ],
    'button' => 'Pelajari Lebih Lanjut',
    'newsletter' => [
        'title' => 'Berlangganan Buletin',
        'desc' => "Mari Tetap Berhubungan",
        'form' => 'Masukan Email Anda',
        'button' => 'Berlangganan Sekarang'
    ],
    'cantas' => [
        'desc' => 'Smartphone meroket sebagai perangkat pilihan untuk hampir setiap jenis transaksi. Tak terkecuali tol, terutama di kalangan Milenial. Untuk mengimbangi gelombang masa depan ini, Emovis telah merekayasa aplikasi tol berbasis smartphone yang memungkinkan pembayaran tol tanpa tag.',
        'button' => 'Kunjungi Kami'
    ],
    'news' => 'sumber'
];

?>