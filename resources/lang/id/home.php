<?php
return [
    'service' => [
        'title' => 'Layanan',
        'desc' => 'Kami Menawarkan Beragam Layanan',
        'data1' => 'Mengoptimalkan Arus Lalu Lintas',
        'data2' => 'Penurunan Biaya Pengumpulan Tol',
        'data3' => 'Meminimalkan Biaya Ditanggung Pengguna Jalan Tol',
        'data4' => 'Maximizing The Usage Of Existing Infrastructures',
        'data5' => 'Beradaptasi Sepenuhnya Dengan Kondisi Dan Sumber Daya Lokal',
        'data6' => 'Mengintegrasikan Beberapa Metode Pembayaran Dan Mitra'
    ],
    'techno' => [
        'title' => 'Teknologi',
        'desc1' => 'Beberapa',
        'desc2' => 'Implementasi',
        'desc3' => 'Teknologi',
        'desc4' => 'Yang Hebat'
    ],
    'news' => [
        'title' => 'Berita',
        'desc' => 'Setiap Pembaharuan Dari Kami',
    ],
    
    
];

?>