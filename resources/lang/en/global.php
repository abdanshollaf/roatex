<?php
return [
    'header' => [
        'data1' => 'Home',
        'data2' => 'About Us',
        'data3' => 'Solution',
        'data4' => 'Technology',
        'data5' => 'News',
        'data6' => 'Career',
    ],
    'footer1' => [
        'title' => 'COMPANY',
        'desc1' => 'About Us',
        'desc2' => 'Our Work',
        'desc3' => 'Client',
        'desc4' => 'Our Blog',
        'desc5' => 'Contact Us'
    ],
    'footer2' => [
        'title' => 'SERVICES',
        'desc1' => 'MLFF Account',
        'desc2' => 'Get a Cassual Pass',
        'desc3' => 'Installing Tag',
        'desc4' => 'Pay Toll',
        'desc5' => 'Discover Cantas Apps'
    ],
    'button' => 'Read More',
    'newsletter' => [
        'title' => 'Subscribe Newsletter',
        'desc' => "Let's Stay in Touch",
        'form' => 'Enter your email',
        'button' => 'Subscribe Now'
    ],
    'cantas' => [
        'desc' => 'Smartphones are skyrocketing as the device of choice for almost every type of transaction. Tolling is no exception, especially among Millennials. To match this wave of the future, Emovis has engineered a smartphone-based tolling application allowing toll-payment without a tag.',
        'button' => 'Visit Site'
    ],
    'news' => 'source'
];

?>