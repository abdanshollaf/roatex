<?php
return [
    'aboutus' => [
        'title' => 'About Us',
    ],
    'service' => [
        'title' => 'Services',
        'desc' => 'We Offer a Wide Variety
        Of Services.',
        'data1' => 'Optimizing Traffic Flow',
        'data2' => 'Decreasing Toll Collection Cost',
        'data3' => 'Minimizing Costs Borne By Toll Road Users',
        'data4' => 'Maximizing The Usage Of Existing Infrastructures',
        'data5' => 'Adapting Fully To The Local Conditions And Resources',
        'data6' => 'Integrating Several Payment Method And Partners'
    ],
    'techno' => [
        'title' => 'Technology',
        'desc1' => 'Some',
        'desc2' => 'Great',
        'desc3' => 'Technology',
        'desc4' => 'Implementation'
    ],
    'news' => [
        'title' => 'News',
        'desc' => 'Every Single Update From Us',
    ],
    
    
];

?>