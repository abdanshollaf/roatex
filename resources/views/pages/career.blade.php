@extends('layouts.app')

@section('title')
    @lang('global.header.data6')
@endsection

@section('content')
    <section>
        <div class="jumbotron-career jumbotron-fluid">
            <div class="container">
                <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> @lang('global.header.data6') </strong></h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 d-flex align-items-center">
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('homepage') }}"
                                class="nav-link">@lang('global.header.data1')</a> </li>
                        <li class="breadcrumb-item active d-flex" aria-current="page"><a href="{{ route('career-index') }}"
                                class="nav-link">@lang('global.header.data6')</a>
                    </ol>
                </nav>

            </div>
    </section>

    <section>
        <div class="container">
            <aside class="col-11 col-sm-11">
                <section id="list-latest-post">
                    <div class="list-group" style="margin-top:30px;margin-left:25px; margin-bottom: 30px">
                        @foreach ($data as $item)
                            <div class="card">
                                <div class="row">
                                    <div class="col-lg-12 wow fadeInUp" data-wow-delay=".15s">
                                        <p class="hero-font">
                                            @if (Session::get('locale') == 'en')
                                                {{ nl2br($item->department) }}
                                            @else
                                                {{ nl2br($item->department_ind) }}
                                            @endif
                                        </p>
                                        <h4 class="hero-font">
                                            @if (Session::get('locale') == 'en')
                                                {{ nl2br($item->title) }}
                                            @else
                                                {{ nl2br($item->title_ind) }}
                                            @endif
                                        </h4>
                                        <p class="hero-font"
                                            style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">
                                            @if (Session::get('locale') == 'en')
                                                {{ nl2br($item->desc) }}
                                            @else
                                                {{ nl2br($item->desc_ind) }}
                                            @endif
                                        </p>
                                        <a href="{{ route('career-show', $item->id) }}" class="btn btn-warning hero-font">
                                            Detail</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </section>
            </aside>
        </div>
    </section>
@endsection
