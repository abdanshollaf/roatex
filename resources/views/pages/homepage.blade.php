@extends('layouts.app')

@section('title')
    {{-- {{ trans('global.header.data1') }} --}}
    {{-- @lang('global.header.data1') --}}
@endsection

@section('content')
    <section id="hero" style="background-color: #FFF7F4;" class="hero-font">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-5" style="margin-top: 50px">
                    <div class="wow fadeInLeft" data-wow-delay=".3s">
                        <h1 class="text-mobile">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[0]->title) !!}
                            @else
                                {!! nl2br($data[0]->title_ind) !!}
                            @endif
                        </h1>
                        <p style="font-weight: 200px">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[0]->desc) !!}
                            @else
                                {!! nl2br($data[0]->desc_ind) !!}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-7" style="margin-top: 50px; margin-bottom: 50px;">
                    <div class="wow fadeInRight" data-wow-delay=".3s">
                        <img loading="lazy" style="max-height: 350px; width: 100%; object-fit: cover" alt=""
                            src={{ asset('' . $data[0]->image) }} class="w-100 float-right">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="about-us" class="hero-font">
        <div class="container">
            <div class="row" style="margin-top: 50px; margin-bottom: 50px">
                <div class="col-sm-12 col-md-12 col-lg-6" style="positon:absolute">
                    <div class="embed-responsive embed-responsive-16by9 wow fadeInLeft" data-wow-delay=".5s">
                        <video controls src="{{ asset($aboutus[0]->video) }}" class="embed-responsive-item" width="100%"
                            max-height="350px"></video>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6" style="positon:absolute; padding-left: 10px">
                    <div class="wow fadeInRight" data-wow-delay=".5s">
                        <h5 style="color:#7B7B7B">
                            @lang('home.aboutus.title')
                        </h5>
                        <h1 class="text-mobile">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($aboutus[0]->title) !!}
                            @else
                                {!! nl2br($aboutus[0]->title_ind) !!}
                            @endif

                        </h1>
                        <p
                            style="font-weight: 200px; display: -webkit-box;overflow: hidden;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($aboutus[0]->desc) !!}
                            @else
                                {!! nl2br($aboutus[0]->desc_ind) !!}
                            @endif
                        </p>
                        <a href="{{ route('aboutus-index') }}"
                            class="btn btn-warning text-white shadow">@lang('global.button')</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="services" style="background-color: #FFF7F4;" class="hero-font">
        <div class="container">
            <div class="row">
                <div class="wow fadeInDown" data-wow-delay=".10s">
                    <h5 style="margin-top:40px; color: #F1A501">
                        @lang('home.service.title')
                    </h5>
                    <div class="col-sm-12">
                        <h1 class="text-mobile">
                            @lang('home.service.desc')
                        </h1>
                    </div>
                </div>
                <div class="row justify-content-center" style="padding: 10px">
                    <div class="col-sm-12 col-md-6 col-lg-4" style="padding: 20px">
                        <div class="wow zoomIn" data-wow-delay=".5s" style="background-color: #FFF7F4; border:none">
                            <div class="d-flex justify-content-center" style="positon:absolute">
                                <img src={{ URL('assets/images/Icon/Icon1.png') }} alt=""
                                    style="width: 80px; height: 80px;">
                            </div>
                            <p class="text-black text-center">
                                <strong>@lang('home.service.data1')</strong>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="padding: 20px">
                        <div class="wow zoomIn" data-wow-delay=".5s" style="background-color: #FFF7F4; border:none">
                            <div class="d-flex justify-content-center" style="positon:absolute">
                                <img src={{ URL('assets/images/Icon/Icon2.png') }} alt=""
                                    style="width: 80px; height: 80px;">
                            </div>
                            <p class="text-black text-center">
                                <strong>@lang('home.service.data2')</strong>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="padding: 20px">
                        <div class="wow zoomIn" data-wow-delay=".5s" style="background-color: #FFF7F4;  border:none">
                            <div class="d-flex justify-content-center" style="positon:absolute">
                                <img src={{ URL('assets/images/Icon/Icon3.png') }} alt=""
                                    style="width: 80px; height: 80px;">
                            </div>
                            <p class="text-black text-center">
                                <strong>@lang('home.service.data3')</strong>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="padding: 20px">
                        <div class="wow zoomIn" data-wow-delay=".5s" style="background-color: #FFF7F4; border:none">
                            <div class="d-flex justify-content-center" style="positon:absolute">
                                <img src={{ URL('assets/images/Icon/Icon1.png') }} alt=""
                                    style="width: 80px; height: 80px;">
                            </div>
                            <p class="text-black text-center">
                                <strong>@lang('home.service.data4')</strong>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="padding: 20px">
                        <div class="wow zoomIn" data-wow-delay=".5s" style="background-color: #FFF7F4; border:none">
                            <div class="d-flex justify-content-center" style="positon:absolute">
                                <img src={{ URL('assets/images/Icon/Icon2.png') }} alt=""
                                    style="width: 80px; height: 80px;">
                            </div>
                            <p class="text-black text-center">
                                <strong>@lang('home.service.data5')</strong>
                            </p>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-6 col-lg-4" style="padding: 20px">
                        <div class="wow zoomIn" data-wow-delay=".5s" style="background-color: #FFF7F4;  border:none">
                            <div class="d-flex justify-content-center" style="positon:absolute">
                                <img src={{ URL('assets/images/Icon/Icon3.png') }} alt=""
                                    style="width: 80px; height: 80px;">
                            </div>
                            <p class="text-black text-center" style="text-align: left">
                                <strong>@lang('home.service.data6')</strong>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="technology" class="hero-font">
        <div class="container">
            <div class="row">
                <div class="wow zoomIn" data-wow-delay=".5s">
                    <div class="d-flex justify-content-center" style="margin-top: 50px">
                        <h5 style="color: #7B7B7B">@lang('home.techno.title')</h5>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 20px">
                        <h1 class="text-center text-mobile">@lang('home.techno.desc1') <span style="color: #F1A501">
                                @lang('home.techno.desc2') <br>
                                @lang('home.techno.desc3')</span>
                            @lang('home.techno.desc4')</h1>
                    </div>
                </div>
                <div class="d-flex justify-content-between row" style="margin-bottom:50px">
                    <div class="col-sm-12 col-md-12 col-lg-5">
                        <div class="card wow fadeInUp" data-wow-delay=".5s" style="background-color: #F1A501">
                            <img src={{ asset($data[2]->image) }} alt=""
                                style="width: 80px; height: 80px;margin-left: -30px; margin-top: 30px; position:absolute">
                            <div class="card-body">
                                <h5 class="text-white"
                                    style="text-align: left; margin-left: 50px; margin-top: 30px; display: -webkit-box;overflow: hidden;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
                                    @if (Session::get('locale') == 'en')
                                        {!! nl2br($data[2]->title) !!}
                                    @else
                                        {!! nl2br($data[2]->title_ind) !!}
                                    @endif
                                </h5>
                                <p class="text-white"
                                    style="margin-left: 50px; margin-top: 20px; text-align: justify; display: -webkit-box;overflow: hidden;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                                    @if (Session::get('locale') == 'en')
                                        {!! nl2br($data[2]->desc) !!}
                                    @else
                                        {!! nl2br($data[2]->desc_ind) !!}
                                    @endif

                                </p>
                                <a href="{{ route('homepage-show', $data[2]->id) }}"
                                    class="btn btn-secondary btn-sm shadow border-0" style="margin-left: 50px;">
                                    @lang('global.button')</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-5">
                        <div class="card wow fadeInDown" data-wow-delay=".5s" style="background-color: #F1A501">
                            <img src={{ asset($data[3]->image) }} alt=""
                                style="width: 80px; height: 80px;margin-left: -30px; margin-top: 30px; position:absolute">
                            <div class="card-body">
                                <h5 class="text-white"
                                    style="text-align: left; margin-left: 50px; margin-top: 30px; display: -webkit-box;overflow: hidden;-webkit-line-clamp: 1;-webkit-box-orient: vertical;">
                                    @if (Session::get('locale') == 'en')
                                        {!! nl2br($data[3]->title) !!}
                                    @else
                                        {!! nl2br($data[3]->title_ind) !!}
                                    @endif
                                </h5>
                                <p class="text-white"
                                    style="margin-left: 50px; margin-top: 20px; text-align: justify; display: -webkit-box;overflow: hidden;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                                    @if (Session::get('locale') == 'en')
                                        {!! nl2br($data[3]->desc) !!}
                                    @else
                                        {!! nl2br($data[3]->desc_ind) !!}
                                    @endif
                                </p>
                                <a href="{{ route('homepage-show', $data[3]->id) }}"
                                    class="btn btn-secondary btn-sm shadow border-0" style="margin-left: 50px;">
                                    @lang('global.button')</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="news" style="background-color: #FFF7F4; positon:absolute" class="hero-font">
        <div class="container">
            <div class="row">
                <div class="wow fadeInUp" data-wow-delay=".5s">
                    <div class="d-flex justify-content-center" style="margin-top: 50px; positon:absolute">
                        <h5 style="color: #F1A501">@lang('home.news.title')</h5>
                    </div>
                    <div class="d-flex justify-content-center" style="margin-top: 20px; positon:absolute">
                        <h1 class="text-center text-mobile"> @lang('home.news.desc')</h1>
                    </div>
                </div>
                <div class="d-flex justify-content-center row" style="margin-bottom:50px;">
                    @foreach ($news as $item)
                        <div class="col-sm-12 col-md-12 col-lg-4 text-left ">
                            <div class="card wow zoomIn shadow border-0" data-wow-delay=".5s"
                                style="background-color: white">
                                <div class="row">
                                    <img src={{ $item->image }} alt=""
                                        style="padding-top: 10px; height: 300px; width: 100%;overflow: hidden; object-fit: cover;">
                                </div>
                                <div class="card-body" style="height: 240px">
                                    <h5 class="text-black" style="text-align: left; margin-top: 10px">
                                        @if (Session::get('locale') == 'en')
                                            {{ nl2br($item->title) }}
                                        @else
                                            {{ nl2br($item->title_ind) }}
                                        @endif
                                    </h5>
                                    <p class="text-black"
                                        style="margin-top: 20px; text-align: justify; display: -webkit-box;overflow: hidden;-webkit-line-clamp: 6;-webkit-box-orient: vertical;">
                                        @if (Session::get('locale') == 'en')
                                            {{ nl2br($item->desc) }}
                                        @else
                                            {{ nl2br($item->desc_ind) }}
                                        @endif
                                    </p>

                                </div>
                                <div class="card-footer"
                                    style="color: white; background-color: white; border-top: white;">
                                    <a style="" href="{{ route('news-show', $item->id) }}"
                                        class="btn btn-warning text-white shadow border-0">@lang('global.button')</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </section>

    <section id="cantas" class="hero-font">
        <div class="container" style="positon:absolute ">
            <div class="card wow zoomIn border-0" style="background-color: #F1F8FD;" data-wow-delay=".5s">
                <div class="row">
                    <div class="col-sm-12 col-md-5 col-lg-5 wow fadeInUp" data-wow-delay=".10s">
                        <img style="height: 100%; width: 100%; object-fit: cover"
                            src={{ URL('assets/images/cantasimg.png') }} alt="">
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-7 wow fadeInLeft" data-wow-delay=".10s"
                        style="padding-left: 10px">
                        <img style="width: 217px; height:64px; margin-top: 15px; positon:absolute"
                            src={{ URL('assets/images/canstasclear.png') }} alt="">
                        <p style="positon:absolute">@lang('global.cantas.desc')</p>
                        <div class="cantas-smartphone">
                            <div class="cantas-visit">
                                <a style="positon:absolute" target="_blank" href="https://cantas.id/id"
                                    class="btn btn-warning text-white">@lang('global.cantas.button')</a>
                            </div>
                            <div class="d-flex g-3 cantas-app">
                                <a href="https://cantas.id">
                                    <img src={{ URL('assets/images/Playstore.png') }} alt=""
                                        style="width: 120px; height: 40px;">
                                </a>
                                <a href="https://cantas.id">
                                    <img src={{ URL('assets/images/Googleplay.png') }} alt=""
                                        style="width: 120px; height: 40px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="subscribe" class="hero-font">
        <div class="container" style="margin-top:50px; margin-bottom:50px  ">
            <div class="card" style="background-color: #F1A501; width: 100%;">
                <div class="row justify-content-center" style="padding: 30px">
                    <div class="d-flex row">
                        <div class="col-sm-12 col-md-5 col-lg-5">
                            <span class="text-white">@lang('global.newsletter.title')</span>
                            <h4 class="text-white"><strong> @lang('global.newsletter.desc')</strong></h4>
                        </div>
                        <div class="col-sm-12 col-md-7 col-lg-7">
                            <div class="input-group">
                                <input type="email" style="height:60px" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="@lang('global.newsletter.form')"
                                    aria-label="Recipient's username">
                                <button style="height: 60px" class="btn btn-outline-light text-white" type="button"
                                    id="button-addon2">@lang('global.newsletter.button')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
