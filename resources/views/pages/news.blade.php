@extends('layouts.app')

@section('title')
    @lang('global.header.data5')
@endsection

@section('content')
    <section>
        <div class="jumbotron-news jumbotron-fluid">
            <div class="container">
                <h1 class="display-4" style="margin-top:50px"><strong> @lang('global.header.data5') </strong></h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 d-flex align-items-center" style="">
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('homepage') }}"
                                class="nav-link">@lang('global.header.data1')</a> </li>
                        <li class="breadcrumb-item active d-flex" aria-current="page"><a href="{{ route('news-index') }}"
                                class="nav-link">@lang('global.header.data5')</a>
                    </ol>
                </nav>

            </div>
    </section>

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-8">
                    @foreach ($dataAll as $item)
                        <div class="wow fadeIn" data-wow-delay=".10s">
                            <div class="d-flex justify-content-center wow fadeInDown" data-wow-delay=".20s">
                                <img class="rounded d-block image-news" alt=""src={{ asset($item->image) }}
                                    style="margin-top: 30px; object-fit: cover;">
                            </div>
                            <div class="card wow fadeInUp" data-wow-delay=".30s" style="margin-top: 10px; border:none">
                                <h3 class="hero-font" style="font-size: 25px; font-weight: 600;">
                                    @if (Session::get('locale') == 'en')
                                        {!! nl2br($item->title) !!}
                                    @else
                                        {!! nl2br($item->title_ind) !!}
                                    @endif
                                </h3>
                                <p
                                    style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 10;-webkit-box-orient: vertical;">
                                    @if (Session::get('locale') == 'en')
                                        {!! nl2br($item->desc) !!}
                                    @else
                                        {!! nl2br($item->desc_ind) !!}
                                    @endif
                                </p>
                                <a href={{ route('news-show', $item->id) }} class="btn btn-warning text-white"
                                    style="width:150px; height:45px"> <span style="font-size:20px">Baca</span> </a>
                            </div>
                        </div>
                    @endforeach
                </div>
                <aside class="col-sm-12 col-md-12 col-lg-4 wow fadeInRight" data-wow-delay=".40s"
                    style="margin-top: 30px; padding-bottom: 50px; padding-left: 25px">
                    <section id="search">
                        <nav class="navbar navbar-light bg-light">
                            <div class="container-fluid">
                                <form class="d-flex" method="GET" action="{{ route('news-index') }}">
                                    @csrf
                                    <input class="form-control me-2" type="text" placeholder="Search" aria-label="Search"
                                        name="search" value="{{ $search }}">
                                    <button class="btn btn-outline-success" type="submit">Search</button>
                                </form>
                            </div>
                        </nav>
                    </section>

                    <section id="list-latest-post">
                        <div class="list-group" style="margin-top:30px">
                            <h3>Latest Post</h3>
                            @foreach ($dataLimit as $item)
                                <a href="{{ route('news-show', $item->id) }}" class="list-group-item">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <img src={{ url('assets/images/Icon/Icon1.png') }} alt=""
                                                style="max-height: 100%; max-width: 100%">
                                        </div>
                                        <div class="col-lg-8" style="margin-top:20px">
                                            <h4>
                                                @if (Session::get('locale') == 'en')
                                                    {!! nl2br($item->title) !!}
                                                @else
                                                    {!! nl2br($item->title_ind) !!}
                                                @endif
                                            </h4>
                                            <p
                                                style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 3;-webkit-box-orient: vertical;">
                                                @if (Session::get('locale') == 'en')
                                                    {!! nl2br($item->desc) !!}
                                                @else
                                                    {!! nl2br($item->desc_ind) !!}
                                                @endif
                                            </p>
                                        </div>
                                    </div>
                                </a>
                            @endforeach
                        </div>
                    </section>
                </aside>
            </div>
        </div>
        <div class="d-flex justify-content-center" style="color: #ffc107">
            {!! $dataAll->links() !!}
        </div>
    </section>
@endsection
