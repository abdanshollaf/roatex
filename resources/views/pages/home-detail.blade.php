@extends('layouts.app')

@section('title')
    @lang('global.header.data1')
@endsection

@section('content')
    <section>
        <div class="jumbotron-home hero-font">
            <div class="container">
                <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> @lang('global.header.data1') </strong></h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 d-flex align-items-center">
                        <li class="breadcrumb-item active" aria-current="page"><a class="nav-link"
                                href="{{ route('homepage') }}">@lang('global.header.data1')</a> </li>
                        <li class="breadcrumb-item active d-flex" aria-current="page"><a class="nav-link"
                                href="{{ route('homepage-show', $data->id) }}">Detail</a></li>
                    </ol>
                </nav>

            </div>
    </section>

    <section>
        <div class="container hero-font">
            <div class="col-lg-10">
                <img class="rounded mx-auto d-block" src={{ asset($data->image) }} alt="">
                <div class="card border-0">
                    <h3 class="hero-font" style="font-size: 25px; font-weight: 600;">
                        @if (Session::get('locale') == 'en')
                            {!! nl2br($data->title) !!}
                        @else
                            {!! nl2br($data->title_ind) !!}
                        @endif
                    </h3>
                    <p style="white-space: pre-wrap;">
                        @if (Session::get('locale') == 'en')
                            {!! nl2br($data->desc) !!}
                        @else
                            {!! nl2br($data->desc_ind) !!}
                        @endif
                    </p>
                </div>
            </div>
    </section>
@endsection
