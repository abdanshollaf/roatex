@extends('layouts.app')

@section('title')
    @lang('global.header.data6')
@endsection

@section('content')
    <section>
        <div class="jumbotron-career jumbotron-fluid">
            <div class="container">
                <h1 class="display-4" style="margin-left: 30px; margin-top:50px"><strong> @lang('global.header.data6') </strong></h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 d-flex align-items-center">
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('homepage') }}"
                                class="nav-link">@lang('global.header.data1')</a> </li>
                        <li class="breadcrumb-item active d-flex" aria-current="page"><a href="{{ route('career-index') }}"
                                class="nav-link">@lang('global.header.data6')</a>
                    </ol>
                </nav>
            </div>
    </section>
    <section>
        <div class="container">
            <div class="col-lg-10">
                <div class="card" style="margin-top: 10px; margin-left:80px; border:none">
                    <p class="hero-font">
                        @if (Session::get('locale') == 'en')
                            {!! $data->department !!}
                        @else
                            {!! $data->department_ind !!}
                        @endif

                    </p>
                    <h3 class="hero-font" style="font-size: 25px; font-weight: 600;">
                        @if (Session::get('locale') == 'en')
                            {!! $data->title !!}
                        @else
                            {!! $data->title_ind !!}
                        @endif
                    </h3>
                    <p class="hero-font">
                        @if (Session::get('locale') == 'en')
                            {!! $data->desc !!}
                        @else
                            {!! $data->desc_ind !!}
                        @endif

                    </p>
                </div>
            </div>
    </section>
@endsection
