@extends('layouts.app')

@section('title')
Services
@endsection

@section ('content')

<section id="services">
    <h1 class="text-center">SERVICES</h1>

    <p class="text-center">Leveraging the latest technological advances, we design, develop and operate e-toll systems, with these main objectives:</p>
</section>

<section>
   <div class="container">
    <div class="row">
        <div class="col index-services">
            <div class="card" style="background-color: #FFFFFF;  border:none">

                    <div class="d-flex justify-content-center" style="positon:absolute">
                        <img src={{URL('assets/images/Icon/Icon1.png')}} alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                    </div>
                    <p style="" class="text-black text-center" style="text-align: left; margin-left: 20px; margin-top: 10px" > <strong>Optimizing Traffic Flow</strong> </p>
                        <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Main objective of our e-toll systems is to avoid congestion and optimize the flow of traffic ensuring a free flow of vehicles on the road.</p>

            </div>
        </div>
        <div class="col index-services">
            <div class="card" style="background-color: #FFFFFF;  border:none">

                    <div class="d-flex justify-content-center" style="positon:absolute">
                        <img src={{URL('assets/images/Icon/Icon1.png')}} alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                    </div>
                    <p class="text-black text-center" style="text-align: left; margin-left: 20px; margin-top: 10px" > <strong> Decreasing Toll Collection Cost</strong> </p>
                    <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">Our aim is to design financially feasible e-toll systems, which using the right technology may not only decrease the toll collection costs, but also increase the income due to the efficiency of the system.</p>

            </div>
        </div>
        <div class="col index-services">
            <div class="card" style="background-color: #FFFFFF;  border:none">

                    <div class="d-flex justify-content-center" style="positon:absolute">
                        <img src={{URL('assets/images/Icon/Icon1.png')}}  alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                    </div>
                    <p class="text-black text-center" style="text-align:; margin-left: 20px; margin-top: 10px" > <strong>Minimizing Costs Borne By Toll Road Users</strong> </p>
                    <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">We deliver the best possible e-toll solutions to road owners, operators and users alike. It’s one of our main goals to create user friendly, convenient e-toll systems, which at the same time do not need to result in higher prices.</p>
            </div>
        </div>
        <div class="col index-services">
            <div class="card" style="background-color: #FFFFFF;  border:none">

                    <div class="d-flex justify-content-center" style="positon:absolute">
                        <img src={{URL('assets/images/Icon/Icon1.png')}}  alt="" style="width: 80px; height: 80px;margin-left: 5px; margin-top: 10px;">
                    </div>
                    <p class="text-black text-center" style="text-align:; margin-left: 20px; margin-top: 10px" > <strong>Minimizing Costs Borne By Toll Road Users</strong> </p>
                    <p class="text-black" style="margin-left: 20px; margin-top: 20px; text-align: justify">We deliver the best possible e-toll solutions to road owners, operators and users alike. It’s one of our main goals to create user friendly, convenient e-toll systems, which at the same time do not need to result in higher prices.</p>
            </div>
        </div>

    </div>
   </div>
</section>
@endsection
