@extends('layouts.dashboard.template')

@section('title')
    <title>
        Roatex Admin | News</title>
@endsection

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">News</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">News</li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Edit Data News</h3>
                            </div>
                            <div class="card-body">
                                <form action="update" id="form-news">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="title">Judul Berita</label>
                                                <input type="text" class="form-control" id="title" name="title"
                                                    placeholder="Enter title" value="{{ $data->title }}">
                                                <span id="error-title" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="desc">Isi Berita</label>
                                                <textarea name="desc" id="desc" cols="30" rows="10" placeholder="Enter Description"
                                                    class="form-control">{{ $data->desc }}</textarea>
                                                <span id="error-desc" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="article">Link Berita</label>
                                                <input type="text" class="form-control" id="article" name="article"
                                                    placeholder="Enter article" value="{{ $data->article }}">
                                                <span id="error-article" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="image">Gambar</label>
                                                <div class="input-group">
                                                    <input type="file" class="form-control" id="image" name="image"
                                                        accept="image/png, image/gif, image/jpeg, image/jpg">
                                                    <a href="{{ asset($data->image) }}" target="_blank"
                                                        class="btn btn-success">Link</a>
                                                </div>
                                                <span id="error-image" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="">-- Pilih --</option>
                                                    <option value="0" {{ $data->is_deleted == 0 ? 'selected' : '' }}>
                                                        Aktif</option>
                                                    <option value="1" {{ $data->is_deleted == 1 ? 'selected' : '' }}>
                                                        Non Aktif</option>
                                                </select>
                                                <span id="error-status" class="hide error-text text-red"></span>
                                            </div>
                                            <input type="hidden" name="id" id="id" value="{{ $data->id }}">
                                            <div class="form-group">
                                                <div class="float-right">
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.news.index') }}" class="btn btn-warning"
                                                            style="color: white"><i class="fa fa-arrow-alt-circle-left"></i>
                                                            Kembali
                                                        </a>
                                                        <button type="button" class="btn btn-primary"
                                                            id="submit-news">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
