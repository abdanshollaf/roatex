@extends('layouts.dashboard.template')

@section('title')
    <title>
        Roatex Admin | News</title>
@endsection

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">News</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">News</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data News</h3>
                                <div class="float-right">
                                    <button type="button" class="btn btn-primary d-flex align-items-center gap-2"
                                        data-toggle="modal" data-target="#mymodal">
                                        <i class="fa fa-plus-circle"></i>
                                        Tambah
                                    </button>
                                    <x-modal-input formID="submit-news" class="modal-xl">
                                        @slot('title')
                                            Tambah Berita
                                        @endslot
                                        @slot('form')
                                            <form action="store" id="form-news">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="article">Link Berita</label>
                                                            <input type="text" class="form-control" id="article"
                                                                name="article" placeholder="Enter article" required>
                                                            <span id="error-article" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="title">Judul Berita</label>
                                                            <input type="text" class="form-control" id="title"
                                                                name="title" placeholder="Enter title" required>
                                                            <span id="error-title" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="desc">Isi Berita</label>
                                                            <textarea name="desc" id="desc" cols="30" rows="10" placeholder="Enter Description"
                                                                class="form-control"></textarea>
                                                            <span id="error-desc" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="image">Gambar</label>
                                                            <input type="file" class="form-control" id="image"
                                                                name="image"
                                                                accept="image/png, image/gif, image/jpeg, image/jpg">
                                                            <span id="error-image" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="status">Status</label>
                                                            <select name="status" id="status" class="form-control" required>
                                                                <option value="">-- Pilih --</option>
                                                                <option value="1">Aktif</option>
                                                                <option value="0">Non Aktif</option>
                                                            </select>
                                                            <span id="error-status" class="hide error-text text-red"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        @endslot
                                    </x-modal-input>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatables" class="table table-bordered table-hover table-responsive">
                                    <thead class="text-center" style="width: 100%">
                                        <tr>
                                            <th style="width: 3%">No</th>
                                            <th style="width: 12%">Judul Berita</th>
                                            <th style="width: 25%">
                                                Isi Berita</th>
                                            <th style="width: 7%">Link Berita</th>
                                            <th style="width: 5%">Gambar</th>
                                            <th style="width: 10%">Penulis</th>
                                            <th style="width: 5%">Status</th>
                                            <th style="width: 12%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td style="word-break: break-word"><span
                                                        style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">{{ $item->title }}</span>
                                                </td>
                                                <td style="word-break: break-word"> <span
                                                        style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">{{ $item->desc }}</span>
                                                </td>
                                                <td><a href="{{ $item->article }}" target="_blank" class="btn btn-info">
                                                        Link</a></td>
                                                <td><a href="{{ asset($item->image) }}" target="_blank"
                                                        class="btn btn-secondary"> Link</a>
                                                </td>
                                                <td>{{ $item->user }}</td>
                                                <td>
                                                    @if ($item->is_deleted == 0)
                                                        <span class="badge btn-success">Aktif</span>
                                                    @else
                                                        <span class="badge btn-danger">Non Aktif</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->is_deleted == 0)
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.news.edit', $item->id) }}"
                                                                class="btn btn-primary"><i class="fa fa-pencil-alt"></i>
                                                                Ubah </a>
                                                            <a href="javascript:void(0)" class="btn btn-danger"
                                                                data-id={{ $item->id }} data-action=1
                                                                data-aksi="nonaktifkan" onclick="actionData(this)"><i
                                                                    class="fa fa-ban"></i>
                                                                Nonaktifkan </a>
                                                        </div>
                                                    @else
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.news.edit', $item->id) }}"
                                                                class="btn btn-primary"><i class="fa fa-pencil-alt"></i>
                                                                Ubah </a>
                                                            <a href="javascrip:void(0)" class="btn btn-success"
                                                                data-id={{ $item->id }} data-action=0
                                                                data-aksi="aktifkan" onclick="actionData(this)"><i
                                                                    class="fa fa-ban"></i>
                                                                Aktifkan </a>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
