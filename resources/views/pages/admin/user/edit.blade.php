@extends('layouts.dashboard.template')

@section('title')
    <title>
        Roatex Admin | Users</title>
@endsection

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Users</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Edit Data Users</h3>
                            </div>
                            <div class="card-body">
                                <form action="update" id="form-user">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="name">Nama</label>
                                                <input type="text" class="form-control" id="name" name="name"
                                                    placeholder="Enter name" value="{{ $data->name }}" required>
                                                <span id="error-name" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Alamat Email</label>
                                                <input type="email" class="form-control" id="email" name="email"
                                                    placeholder="Enter email" value="{{ $data->email }}" required>
                                                <span id="error-email" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input type="password" class="form-control" id="password" name="password">
                                                <span id="error-password" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="status">Status</label>
                                                <select name="status" id="status" class="form-control">
                                                    <option value="">-- Pilih --</option>
                                                    <option value="1" {{ $data->is_active == 1 ? 'selected' : '' }}>
                                                        Aktif</option>
                                                    <option value="0" {{ $data->is_active == 0 ? 'selected' : '' }}>Non
                                                        Aktif</option>
                                                </select>
                                                <span id="error-status" class="hide error-text text-red"></span>
                                            </div>
                                            <input type="hidden" name="id" id="id" value="{{ $data->id }}">
                                            <div class="form-group">
                                                <div class="float-right">
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.users.index') }}" class="btn btn-warning"
                                                            style="color: white"><i class="fa fa-arrow-alt-circle-left"></i>
                                                            Kembali
                                                        </a>
                                                        <button type="button" class="btn btn-primary"
                                                            id="submit-user">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
