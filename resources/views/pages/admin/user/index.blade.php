@extends('layouts.dashboard.template')

@section('title')
    <title>
        Roatex Admin | Users</title>
@endsection

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Users</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Users</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Users</h3>
                                <div class="float-right">
                                    <button type="button" class="btn btn-primary d-flex align-items-center gap-2"
                                        data-toggle="modal" data-target="#mymodal">
                                        <i class="fa fa-plus-circle"></i>
                                        Tambah
                                    </button>
                                    <x-modal-input formID="submit-user">
                                        @slot('title')
                                            Tambah User
                                        @endslot
                                        @slot('form')
                                            <form action="store" id="form-user">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="name">Nama</label>
                                                            <input type="text" class="form-control" id="name"
                                                                name="name" placeholder="Enter name">
                                                            <span id="error-name" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="email">Alamat Email</label>
                                                            <input type="email" class="form-control" id="email"
                                                                name="email" placeholder="Enter email">
                                                            <span id="error-email" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="password">Password</label>
                                                            <input type="password" class="form-control" id="password"
                                                                name="password" placeholder="Enter password">
                                                            <span id="error-password" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="status">Status</label>
                                                            <select name="status" id="status" class="form-control">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="1">Aktif</option>
                                                                <option value="0">Non Aktif</option>
                                                            </select>
                                                            <span id="error-status" class="hide error-text text-red"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        @endslot
                                    </x-modal-input>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatables" class="table table-bordered table-hover table-responsive">
                                    <thead class="text-center" style="width: 100%">
                                        <tr>
                                            <th style="width: 5%">No</th>
                                            <th style="width: 25%">Nama</th>
                                            <th style="width: 25%">Email</th>
                                            <th style="width: 25%">Status</th>
                                            <th style="width: 25%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->name }}</td>
                                                <td>{{ $item->email }}</td>
                                                <td>
                                                    @if ($item->is_active == 1)
                                                        <span class="badge btn-success">Aktif</span>
                                                    @else
                                                        <span class="badge btn-danger">Non Aktif</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->is_active == 1)
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.users.edit', $item->id) }}"
                                                                class="btn btn-primary"><i class="fa fa-pencil-alt"></i>
                                                                Ubah </a>
                                                            <a href="javascript:void(0)" class="btn btn-danger"
                                                                data-id={{ $item->id }} data-action=0
                                                                data-aksi="nonaktifkan user ini"
                                                                onclick="actionData(this)"><i class="fa fa-ban"></i>
                                                                Nonaktifkan </a>
                                                        </div>
                                                    @else
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.users.edit', $item->id) }}"
                                                                class="btn btn-primary"><i class="fa fa-pencil-alt"></i>
                                                                Ubah </a>
                                                            <a href="javascrip:void(0)" class="btn btn-success"
                                                                data-id={{ $item->id }} data-action=1
                                                                data-aksi="aktifkan user ini" onclick="actionData(this)"><i
                                                                    class="fa fa-ban"></i>
                                                                Aktifkan </a>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
