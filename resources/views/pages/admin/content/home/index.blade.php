@extends('layouts.dashboard.template')

@section('title')
    <title>Roatex Admin | Content - Home</title>
@endsection

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Content - Home</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Content</li>
                            <li class="breadcrumb-item active">Home</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Content - Home</h3>
                            </div>
                            <div class="card-body">
                                <table id="datatables" class="table table-bordered table-hover table-responsive">
                                    <thead class="text-center" style="width: 100%">
                                        <tr>
                                            <th style="width: 5%">No</th>
                                            <th style="width: 10%">Letak Baris</th>
                                            <th style="width: 20%">Judul</th>
                                            <th style="width: 42%">Deskripsi</th>
                                            <th style="width: 8%">Gambar</th>
                                            <th style="width: 7%">Video</th>
                                            <th style="width: 25%">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->row }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td> <span
                                                        style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">
                                                        {{ $item->desc }}
                                                    </span>
                                                </td>
                                                <td>
                                                    @if ($item->image != null)
                                                        <a href="{{ asset($item->image) }}" target="_blank"
                                                            class="btn btn-secondary"> Link</a>
                                                    @endif

                                                </td>
                                                <td>
                                                    @if ($item->video != null)
                                                        <a href="{{ asset($item->video) }}" target="_blank"
                                                            class="btn btn-secondary"> Link</a>
                                                    @endif

                                                </td>
                                                <td>
                                                    <a href="{{ route('admin.content.home.edit', $item->id) }}"
                                                        class="btn btn-primary"><i class="fa fa-pencil-alt"></i>
                                                        Ubah </a>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
