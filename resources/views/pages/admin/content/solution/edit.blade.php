@extends('layouts.dashboard.template')

@section('title')
    <title>
        Roatex Admin | Content - Home</title>
@endsection

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Content - Solution</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Content</li>
                            <li class="breadcrumb-item active">Solution</li>
                            <li class="breadcrumb-item active">Edit</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Edit Data Content - Solution</h3>
                            </div>
                            <div class="card-body">
                                <form action="update" id="form-content">
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label for="row">Baris</label>
                                                <input type="text" class="form-control" id="row"
                                                    name="row"value="{{ $data->row }}" readonly>
                                                <span id="error-row" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="title">Judul</label>
                                                <input type="text" class="form-control" id="title" name="title"
                                                    placeholder="Enter title" value="{{ $data->title }}">
                                                <span id="error-title" class="hide error-text text-red"></span>
                                            </div>
                                            <div class="form-group">
                                                <label for="desc">Deskripsi</label>
                                                <textarea name="desc" id="desc" cols="30" rows="10" placeholder="Enter Description"
                                                    class="form-control">{{ $data->desc }}</textarea>
                                                <span id="error-desc" class="hide error-text text-red"></span>
                                            </div>
                                            @if ($data->desc2 != null)
                                                <div class="form-group">
                                                    <label for="desc">Deskripsi 2</label>
                                                    <textarea name="desc2" id="desc2" cols="30" rows="10" placeholder="Enter Description 2"
                                                        class="form-control">{{ $data->desc2 }}</textarea>
                                                </div>
                                            @endif
                                            @if ($data->image != null)
                                                <div class="form-group">
                                                    <label for="image">Gambar</label>
                                                    <div class="input-group">
                                                        <input type="file" class="form-control" id="image"
                                                            name="image"accept="image/png, image/gif, image/jpeg, image/jpg">
                                                        <a href="{{ asset($data->image) }}" target="_blank"
                                                            class="btn btn-success">Link</a>
                                                    </div>
                                                    <span id="error-image" class="hide error-text text-red"></span>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <label for="video">Video</label>
                                                    <div class="input-group">
                                                        <input type="file" class="form-control" id="video"
                                                            name="video"
                                                            accept="video/mp4,video/mkv, video/x-m4v,video/*">
                                                        <a href="{{ asset($data->video) }}" target="_blank"
                                                            class="btn btn-success">Link</a>
                                                    </div>
                                                    <span id="error-video" class="hide error-text text-red"></span>
                                                </div>
                                            @endif
                                            <input type="hidden" name="id" id="id" value="{{ $data->id }}">
                                            <input type="hidden" name="content_type" id="content_type"
                                                value="{{ $data->content_type }}">
                                            <div class="form-group">
                                                <div class="float-right">
                                                    <div class="btn-group">
                                                        <a href="{{ route('admin.content.solution.index') }}"
                                                            class="btn btn-warning" style="color: white"><i
                                                                class="fa fa-arrow-alt-circle-left"></i>
                                                            Kembali
                                                        </a>
                                                        <button type="button" class="btn btn-primary"
                                                            id="submit-content">Simpan</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
