@extends('layouts.dashboard.template')

@section('title')
    <title>
        Roatex Admin | Careers</title>
@endsection

@section('content')
    <meta name="_token" content="{{ csrf_token() }}">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">Careers</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="#">Home</a></li>
                            <li class="breadcrumb-item active">Careers</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Data Careers</h3>
                                <div class="float-right">
                                    <button type="button" class="btn btn-primary d-flex align-items-center gap-2"
                                        data-toggle="modal" data-target="#mymodal">
                                        <i class="fa fa-plus-circle"></i>
                                        Tambah
                                    </button>
                                    <x-modal-input formID="submit-career" class="modal-xl">
                                        @slot('title')
                                            Tambah Karir
                                        @endslot
                                        @slot('form')
                                            <form action="store" id="form-career">
                                                @csrf
                                                <div class="row">
                                                    <div class="col-12">
                                                        <div class="form-group">
                                                            <label for="department">Departemen / Divisi (Inggris)</label>
                                                            <input type="text" class="form-control" id="department"
                                                                name="department" placeholder="Enter department">
                                                            <span id="error-department" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="department">Departemen / Divisi (Indonesia)</label>
                                                            <input type="text" class="form-control" id="department_ind"
                                                                name="department_ind" placeholder="Enter department">
                                                            <span id="error-department_ind" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="title">Posisi (Inggris)</label>
                                                            <input type="text" class="form-control" id="title"
                                                                name="title" placeholder="Enter title">
                                                            <span id="error-title" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="title">Posisi (Indonesia)</label>
                                                            <input type="text" class="form-control" id="title_ind"
                                                                name="title_ind" placeholder="Enter title">
                                                            <span id="error-title_ind" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="desc">Deskripsi (Inggris)</label>
                                                            <textarea name="desc" id="desc" cols="30" rows="10" placeholder="Enter Description"
                                                                class="form-control"></textarea>
                                                            <span id="error-desc" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="desc">Deskripsi (Indonesia)</label>
                                                            <textarea name="desc_ind" id="desc_ind" cols="30" rows="10" placeholder="Enter Description"
                                                                class="form-control"></textarea>
                                                            <span id="error-desc_ind" class="hide error-text text-red"></span>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="status">Status</label>
                                                            <select name="status" id="status" class="form-control">
                                                                <option value="">-- Pilih --</option>
                                                                <option value="1">Aktif</option>
                                                                <option value="0">Non Aktif</option>
                                                            </select>
                                                            <span id="error-status" class="hide error-text text-red"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        @endslot
                                    </x-modal-input>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="datatables" class="table table-bordered table-hover table-responsive">
                                    <thead class="text-center" style="width: 100%">
                                        <tr>
                                            <th style="width: 3%">No</th>
                                            <th style="width: 10%">Departemen / Divisi (EN)</th>
                                            <th style="width: 10%">Departemen / Divisi (ID)</th>
                                            <th style="width: 25%">Posisi (EN)</th>
                                            <th style="width: 25%">Posisi (ID)</th>
                                            <th style="width: 25%">Deskripsi (EN)</th>
                                            <th style="width: 25%">Deskripsi (ID)</th>
                                            <th style="width: 10%">Dibuat Oleh</th>
                                            <th style="width: 5%">Status</th>
                                            <th style="width: 15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">
                                        @foreach ($data as $item)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $item->department }}</td>
                                                <td>{{ $item->department_ind }}</td>
                                                <td>{{ $item->title }}</td>
                                                <td>{{ $item->title_ind }}</td>
                                                <td><span
                                                        style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">{{ $item->desc }}</span>
                                                </td>
                                                <td><span
                                                        style="display: -webkit-box;overflow: hidden;-webkit-line-clamp: 5;-webkit-box-orient: vertical;">{{ $item->desc_ind }}</span>
                                                </td>
                                                <td>{{ $item->user }}</td>
                                                <td>
                                                    @if ($item->is_deleted == 0)
                                                        <span class="badge btn-success">Aktif</span>
                                                    @else
                                                        <span class="badge btn-danger">Non Aktif</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if ($item->is_deleted == 0)
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.careers.edit', $item->id) }}"
                                                                class="btn btn-primary"><i class="fa fa-pencil-alt"></i>
                                                                Ubah </a>
                                                            <a href="javascript:void(0)" class="btn btn-danger"
                                                                data-id={{ $item->id }} data-action=1
                                                                data-aksi="onaktifkan" onclick="actionData(this)"><i
                                                                    class="fa fa-ban"></i>
                                                                Nonaktifkan </a>
                                                        </div>
                                                    @else
                                                        <div class="btn-group">
                                                            <a href="{{ route('admin.careers.edit', $item->id) }}"
                                                                class="btn btn-primary"><i class="fa fa-pencil-alt"></i>
                                                                Ubah </a>
                                                            <a href="javascrip:void(0)" class="btn btn-success"
                                                                data-id={{ $item->id }} data-action=0
                                                                data-aksi="aktifkan" onclick="actionData(this)"><i
                                                                    class="fa fa-ban"></i>
                                                                Aktifkan </a>
                                                        </div>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
