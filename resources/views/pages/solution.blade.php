@extends('layouts.app')

@section('title')
    @lang('global.header.data3')
@endsection

@section('content')
    <section>
        <div class="jumbotron-solution jumbotron-fluid">
            <div class="container">
                <h1 class="display-4" style="margin-top:50px"><strong> @lang('global.header.data3') </strong></h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 d-flex justify-content-start">
                        <li class="breadcrumb-item active" aria-current="page"><a class="nav-link"
                                href="{{ route('homepage') }}">@lang('global.header.data1')</a> </li>
                        <li class="breadcrumb-item active d-flex" aria-current="page"><a
                                href="{{ route('solution-index') }}" class="nav-link"> @lang('global.header.data3')</a>
                        </li>
                    </ol>
                </nav>
            </div>
    </section>

    <section id="roatex" style="margin-bottom:50px">
        <div class="container justify-content-center wow zoomIn" data-wow-delay="0.10s">
            <div class="col-lg-10 info-panel-solution justify-content-center">
                <div style="" class="embed-responsive embed-responsive-16by9 justify-content-center">
                    <video controls src="{{ asset($data[0]->video) }}" class="embed-responsive-item-solution" width="100%"
                        max-height="350px"></video>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="card" style="background-color:rgba(241, 165, 1, 0.05); border: 0px; margin-top:25px">
                <div class="row">
                    <div class="wow fadeInUp" data-wow-delay="0.15s">
                        <h2 style="font-size: 39px;font-style: normal;font-weight: 600; margin-top: 10px">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[0]->title) !!}
                            @else
                                {!! nl2br($data[0]->title_ind) !!}
                            @endif
                        </h2>
                    </div>
                </div>
                <div class="row" style="margin-top: 20px;">
                    <div class="col-sm-3 col-12 wow fadeInLeft" data-wow-delay="0.20s">
                        <p style="text-align: right" class="hero-font">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[0]->desc) !!}
                            @else
                                {!! nl2br($data[0]->desc_ind) !!}
                            @endif
                        </p>
                    </div>
                    <div class="col-sm-7 col-12 wow fadeInRight" data-wow-delay="0.20s" style="padding-left: 10px">
                        <p class="hero-font" style="color: #7B7B7B">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[0]->desc2) !!}
                            @else
                                {!! nl2br($data[0]->desc2_ind) !!}
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container" style="margin-top:50px">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 mx-5">
                    <div class="wow fadeInUp" data-wow-delay="0.20s">
                        <h3 class="hero-font"
                            style="font-weight: 500;font-size: 25px;font-style: normal; text-align: right">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[1]->title) !!}
                            @else
                                {!! nl2br($data[1]->title_ind) !!}
                            @endif
                        </h3>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay="0.20s">
                        <p class="hero-font" style="text-align: right">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[1]->desc) !!}
                            @else
                                {!! nl2br($data[1]->desc_ind) !!}
                            @endif
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-4">
                    <div class="position-relative w-100 wow fadeInRight container-solution" data-wow-delay=".20s">
                        <div class="position-absolute w-100 h-100 bg-solution"></div>
                        <img src={{ asset($data[1]->image) }} alt=""
                            class="position-absolute w-100 h-100 img-solution">
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container" style="margin-top:50px">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-4 mx-5" style="margin-bottom: 50px">
                    <div class="position-relative w-100 wow fadeInRight container-solution" data-wow-delay=".20s">
                        <div class="position-absolute w-100 h-100 bg-solution"></div>
                        <img src={{ asset($data[2]->image) }} alt=""
                            class="position-absolute w-100 h-100 img-solution">
                    </div>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6">
                    <div class="wow fadeInDown" data-wow-delay=".20s">
                        <h3 style="font-weight: 500;font-size: 25px;font-style: normal;" class="hero-font">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[2]->title) !!}
                            @else
                                {!! nl2br($data[2]->title_ind) !!}
                            @endif
                        </h3>
                    </div>
                    <div class="wow fadeInUp" data-wow-delay=".20s">
                        <p class="hero-font">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[2]->desc) !!}
                            @else
                                {!! nl2br($data[2]->desc_ind) !!}
                            @endif
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="cantas" class="hero-font">
        <div class="container mb-5" style="positon:absolute ">
            <div class="card wow zoomIn border-0" style="background-color: #F1F8FD;" data-wow-delay=".5s">
                <div class="row">
                    <div class="col-sm-12 col-md-5 col-lg-5 wow fadeInUp" data-wow-delay=".10s">
                        <img style="height: 100%; width: 100%; object-fit: cover"
                            src={{ URL('assets/images/cantasimg.png') }} alt="">
                    </div>
                    <div class="col-sm-12 col-md-7 col-lg-7 wow fadeInLeft" data-wow-delay=".10s"
                        style="padding-left: 10px">
                        <img style="width: 217px; height:64px; margin-top: 15px; positon:absolute"
                            src={{ URL('assets/images/canstasclear.png') }} alt="">
                        <p style="positon:absolute">@lang('global.cantas.desc')</p>
                        <div class="cantas-smartphone">
                            <div class="cantas-visit">
                                <a style="positon:absolute" href="https://cantas.id/id" target="_blank"
                                    class="btn btn-warning text-white">@lang('global.cantas.button')</a>
                            </div>
                            <div class="d-flex g-3 cantas-app">
                                <a href="https://cantas.id">
                                    <img src={{ URL('assets/images/Playstore.png') }} alt=""
                                        style="width: 120px; height: 40px;">
                                </a>
                                <a href="https://cantas.id">
                                    <img src={{ URL('assets/images/Googleplay.png') }} alt=""
                                        style="width: 120px; height: 40px;">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
