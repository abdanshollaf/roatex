@extends('layouts.app')

@section('title')
    @lang('global.header.data2')
@endsection

@section('content')
    <section class="hero-font">
        <div class="jumbotron-aboutus jumbotron-fluid">
            <div class="container">
                <h1 class="display-4" style="margin-top:50px"><strong>@lang('global.header.data2') </strong></h1>
                <span>
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb mb-0 d-flex justify-content-start">
                            <li class="breadcrumb-item active" aria-current="page"><a class="nav-link"
                                    href="{{ route('homepage') }}">@lang('global.header.data1')</a> </li>
                            <li class="breadcrumb-item active d-flex" aria-current="page"><a class="nav-link"
                                    href="{{ route('aboutus-index') }}">@lang('global.header.data2')</a>
                            </li>
                        </ol>
                    </nav>
                </span>
            </div>
    </section>

    <!-- Info Panel--->
    <section id="roatex" style="margin-bottom:50px; margin-top: 50px" class="hero-font">
        <div class="container">
            <div class="row justify-content-center wow zoomIn" data-wow-delay=".10s">
                <div class="col-lg-10 info-panel-aboutus">
                    <div class="row">
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6">
                            <div class="embed-responsive embed-responsive-16by9">
                                <video controls src="{{ asset($data[0]->video) }}" class="embed-responsive-item"
                                    width="100%" max-height="350px"></video>
                            </div>
                        </div>
                        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6" style="padding-left: 10px">
                            <h1 style="font-weight: 700; font-size:44px; font-style:normal">
                                @if (Session::get('locale') == 'en')
                                    {!! nl2br($data[0]->title) !!}
                                @else
                                    {!! nl2br($data[0]->title_ind) !!}
                                @endif
                            </h1>
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data[0]->desc) !!}
                            @else
                                {!! nl2br($data[0]->desc_ind) !!}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Our Experioence--->
    <section id="ourexperience" style="background-color: #FFF7F4;" class="hero-font">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-6 wow fadeInLeft" data-wow-delay=".10s"
                    style="margin-top: 70px; positon:absolute">
                    <h5 style="color:#7B7B7B">
                        @lang('aboutus.exp.title')
                    </h5>
                    <h1 style="font-weight: 700; font-size:44px; font-style:normal">
                        @lang('aboutus.exp.desc1') <span style="color: #F1A501">@lang('aboutus.exp.desc2')
                        </span> @lang('aboutus.exp.desc3')

                    </h1>
                    <p style="font-weight: 200px">
                        @lang('aboutus.exp.desc4')</p>
                    <a href="#" class="btn btn-warning text-white">@lang('global.button')</a>
                </div>
                <div class="col-sm-12 col-md-12 col-lg-6" style="padding-bottom: 75px">
                    <div class="col">
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-6">
                                <div class="d-flex justify-content-center">
                                    <div class="card card-aboutus justify-content-center wow fadeInDown"
                                        data-wow-delay=".15s">
                                        <h1 class="text-center hero-font hero-font-title">250+</h1>
                                        <p class="text-center hero-font hero-font-desc"> @lang('aboutus.exp.detail1')</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-6">
                                <div class="d-flex justify-content-center">
                                    <div class="card card-aboutus justify-content-center wow fadeInDown"
                                        data-wow-delay=".15s">
                                        <h1 class="text-center hero-font hero-font-title">156+</h1>
                                        <p class="text-center hero-font hero-font-desc"> @lang('aboutus.exp.detail2')</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6 col-md-6 col-lg-6 col-6">
                                <div class="d-flex justify-content-center">
                                    <div class="card card-aboutus justify-content-center wow fadeInUp"
                                        data-wow-delay=".15s">
                                        <h1 class="text-center hero-font hero-font-title">
                                            50+
                                        </h1>
                                        <p class="text-center hero-font hero-font-desc">@lang('aboutus.exp.detail3')</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 col-md-6 col-lg-6 col-6">
                                <div class="d-flex justify-content-center">
                                    <div class="card card-aboutus justify-content-center wow fadeInUp"
                                        data-wow-delay=".15s">
                                        <h1 class="text-center hero-font hero-font-title">
                                            15+
                                        </h1>
                                        <p class="text-center hero-font hero-font-desc">@lang('aboutus.exp.detail4')</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- End Our Experioence--->
    <section id="subscribe" class="hero-font">
        <div class="container" style="margin-top:50px; margin-bottom:50px  ">
            <div class="card" style="background-color: #F1A501; width: 100%;">
                <div class="row justify-content-center" style="padding: 30px">
                    <div class="d-flex row">
                        <div class="col-sm-12 col-md-5 col-lg-5">
                            <span class="text-white">@lang('global.newsletter.title')</span>
                            <h4 class="text-white"><strong> @lang('global.newsletter.desc') </strong></h4>
                        </div>
                        <div class="col-sm-12 col-md-7 col-lg-7">
                            <div class="input-group">
                                <input type="email" style="height:60px" class="form-control" id="exampleInputEmail1"
                                    aria-describedby="emailHelp" placeholder="@lang('global.newsletter.form')"
                                    aria-label="Recipient's username">
                                <button style="height: 60px" class="btn btn-outline-light text-white" type="button"
                                    id="button-addon2">@lang('global.newsletter.button')</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
