@extends('layouts.app')

@section('title')
    @lang('global.header.data5')
@endsection

@section('content')
    <section>
        <div class="jumbotron-news jumbotron-fluid">
            <div class="container">
                <h1 class="display-4" style="margin-top:50px"><strong> @lang('global.header.data5') </strong></h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 d-flex align-items-center" style="">
                        <li class="breadcrumb-item active" aria-current="page"> <a href="{{ route('homepage') }}"
                                class="nav-link">@lang('global.header.data1')</a> </li>
                        <li class="breadcrumb-item active d-flex" aria-current="page"><a href="{{ route('news-index') }}"
                                class="nav-link">@lang('global.header.data5')</a>
                    </ol>
                </nav>

            </div>
    </section>

    <section>
        <div class="container">
            <div class="col-lg-10">
                <div class="wow fadeIn" data-wow-delay=".10s">
                    <div class="d-flex justify-content-center wow fadeInDown" data-wow-delay=".20s">
                        <img class="rounded d-block image-news" alt=""src={{ asset($data->image) }}
                            style="margin-top: 30px; object-fit: cover;">
                    </div>
                    <div class="card wow fadeInUp" data-wow-delay=".30s" style="margin-top: 10px; border:none">
                        <h3 class="hero-font" style="font-size: 25px; font-weight: 600;">
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data->title) !!}
                            @else
                                {!! nl2br($data->title_ind) !!}
                            @endif
                        </h3>
                        <p>
                            @if (Session::get('locale') == 'en')
                                {!! nl2br($data->desc) !!}
                            @else
                                {!! nl2br($data->desc_ind) !!}
                            @endif
                        </p>
                        <br />
                        <p>@lang('global.news') : <a href="{{ $data->article }}">{{ $data->article }}</a> </p>
                    </div>
                </div>
            </div>
    </section>
@endsection
