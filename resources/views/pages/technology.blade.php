@extends('layouts.app')

@section('title')
    @lang('global.header.data4')
@endsection

@section('content')
    <section>
        <div class="jumbotron-technology jumbotron-fluid">
            <div class="container">
                <h1 class="display-4" style="margin-top:50px"><strong> @lang('global.header.data4') </strong></h1>
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mb-0 d-flex align-items-center">
                        <li class="breadcrumb-item active" aria-current="page"><a class="nav-link"
                                href="{{ route('homepage') }}">@lang('global.header.data1')</a>
                        </li>
                        <li class="breadcrumb-item active d-flex" aria-current="page"><a href="{{ route('tech-index') }}"
                                class="nav-link">@lang('global.header.data4')</a>
                        </li>
                    </ol>
                </nav>

            </div>
    </section>

    <section id="roatex" style="margin-bottom:10px">
        <div class="container justify-content-center wow zoomIn" data-wow-delay=".10s">
            <img class="image-size-tech" style="min-width: 80%; height: 100%;"
                src={{ URL('assets/images/technology/bgimg.png') }} alt="">
        </div>
    </section>
@endsection
