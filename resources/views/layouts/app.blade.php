<!DOCTYPE html>
<html lang={{ Session::get('locale') }}>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="{{ asset('dist/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href={{ asset('style.css') }}>
    <link rel="stylesheet" href={{ asset('animation/css/animate.css') }}>
    <link rel="stylesheet" href={{ asset('animation/css/animate.min.css') }}>
    <link rel="stylesheet" href="{{ asset('dist/css/all.min.css') }}">


    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Work+Sans:wght@400;700&display=swap" rel="stylesheet">

    <style>
        .hero {
            background: #FFF7F4;
            position: absolute;
            width: 1440px;
            height: 719px;
        }

        .about-us {
            background: #FFFFFF;
            position: absolute;
            width: 1440px;
            height: 719px;
        }

        .row {

            /* margin-left: 0.25rem; */
            padding-left: 0.25rem;
            --bs-gutter-x: 0;
        }

        .col-row {
            margin-top: 1rem;
        }

        .card-title {
            margin-left: 1rem;
        }

        .card-text {
            margin-left: 1rem;
        }

        .card-link {
            margin-left: 1rem;
            margin-bottom: 1rem;
        }
    </style>
</head>

<body>
    <nav class="navbar navbar-expand-lg bg-light">
        <div class="container-fluid">
            <a href={{ URL('') }}>
                <img src={{ URL('assets/roatex.png') }} alt="Roatex">
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-3 mb-2 mb-lg-0 mx-auto">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{ route('homepage') }}">@lang('global.header.data1')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('aboutus-index') }}">@lang('global.header.data2')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('solution-index') }}">@lang('global.header.data3')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('tech-index') }}">@lang('global.header.data4')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('news-index') }}">@lang('global.header.data5')</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('career-index') }}">@lang('global.header.data6')</a>
                    </li>
                    <li class="nav-item" style="padding-top: 8px">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                            style="background: transparent; color: var(--bs-nav-link-color); border: none; padding: 0">
                            {{ strtoupper(Session::get('locale')) }}
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton"
                            style="min-width: 0; width: fit-content">
                            <a href="{{ route('lang-en') }}" class="dropdown-item">EN</a>
                            <a href="{{ route('lang-id') }}" class="dropdown-item">ID</a>
                        </div>
                        {{-- <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
                            id="lang" aria-expanded="false" aria-haspopup="true">
                            {{ Session::get('locale') != '' ? "ADA": App::currentLocale() }}
                        </a>
                        <div class="dropdown-menu" aria-labelledby="lang">
                            <a href="{{ route('lang-en') }}" class="dropdown-item">EN</a>
                            <a href="{{ route('lang-id') }}" class="dropdown-item">ID</a>
                        </div> --}}
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    @yield('content')

    <section id="footer" style="background-color:#373737; " class="hero-font">
        <div class="container">
            <div class="row justify-content-start mb-3 mt-3" style="gap: 30px">
                <div class="col-sm-12 col-md-12 col-lg-4 col-xl-4 mt-3 wow fadeInLeft" data-wow-delay=".10s">
                    <div class="row">
                        <a href={{ URL('') }}>
                            <img style="width:193px; height:61px" src={{ URL('assets/images/roatexwhite.png') }}
                                alt="">
                        </a>
                        <p style="margin-top: 30px" class="text-white"> Indonesia Stock Exchange Building Tower II, 19th
                            Floor, Suite 1903, Jl. Jendral Sudirman, Kav 52-53, Lot 2 - Jakarta 12190</p>
                        <div class="col d-flex gap-3">
                            <a href={{ url('') }} style="color: #373737" target="_blank">
                                <img src={{ URL('assets/images/Icon/facebook-fill.png') }} alt="">
                            </a>
                            <a href={{ url('') }} style="color: #373737" target="_blank">
                                <img src={{ URL('assets/images/Icon/instagram-fill.png') }} alt="">
                            </a>
                            <a href={{ url('') }} style="color: #373737" target="_blank">
                                <img src={{ URL('assets/images/Icon/twitter-fill.png') }} alt="">
                            </a>
                            <a href={{ url('') }} style="color: #373737" target="_blank">
                                <img src={{ URL('assets/images/Icon/skype-fill.png') }} alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 col-xl-2 mt-3 wow fadeInUp" data-wow-delay=".10s">
                    <div class="row">
                        <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold" style="margin-left:-200">
                            @lang('global.footer1.title')
                        </h4>
                        <p>
                            <a href="#" class="text-white fw-lighter"
                                style="text-decoration: none;">@lang('global.footer1.desc1')</a>
                        </p>
                        <p>
                            <a href="#" class="text-white fw-lighter"
                                style="text-decoration: none;">@lang('global.footer1.desc2')k</a>
                        </p>
                        <p>
                            <a href="#" class="text-white fw-lighter"
                                style="text-decoration: none;">@lang('global.footer1.desc3')</a>
                        </p>
                        <p>
                            <a href="#" class="text-white fw-lighter"
                                style="text-decoration: none;">@lang('global.footer1.desc4')</a>
                        </p>
                        <p>
                            <a href="#" class="text-white fw-lighter"
                                style="text-decoration: none;">@lang('global.footer1.desc5')</a>
                        </p>

                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 col-xl-2 mt-3 wow fadeInUp" data-wow-delay=".10s">
                    <div class="row">
                        <h4 class="text-uppercase mb-4 font-weight-bold text-white fw-bold">@lang('global.footer2.title')</h4>
                        <p>
                            <a href="#" class="text-white fw-lighter fs-6"
                                style="text-decoration: none;">@lang('global.footer2.desc1')</a>
                        </p>
                        <p>
                            <a href="#" class="text-white fw-lighter fs-6"
                                style="text-decoration: none;">@lang('global.footer2.desc2')</a>
                        </p>
                        <p>
                            <a href="#" class="text-white fw-lighter fs-6"
                                style="text-decoration: none;">@lang('global.footer2.desc3')</a>
                        </p>
                        <p>
                            <a href="#" class="text-white fw-lighter fs-6"
                                style="text-decoration: none;">@lang('global.footer2.desc4')</a>
                        </p>
                    </div>
                </div>
                <div class="col-sm-12 col-md-4 col-lg-2 col-xl-2 mt-3 wow fadeInRight" data-wow-delay=".10s">
                    <div class="row" style="margin-bottom:50px; margin-right: 50px">
                        <p class="text-secondary">@lang('global.footer2.desc5')</p>
                        <a href="https://cantas.id" style="color: #373737" target="_blank">
                            <img style="margin-bottom: 10px" src={{ URL('assets/images/logo.png') }} alt=""
                                width="50%">
                        </a>
                        <div class="col">
                            <a href="https://cantas.id" style="color: #373737" target="_blank">
                                <img style="margin-top:10px" src={{ URL('assets/images/Googleplay.png') }}
                                    alt="">
                            </a>
                            <a href="https://cantas.id" style="color: #373737" target="_blank">
                                <img style="margin-top:10px" src={{ URL('assets/images/Playstore.png') }}
                                    alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="copyright" style="background-color:#F1A501" class="hero-font">
        <div class="container wow fadeInUp" data-wow-delay=".10s">
            <div class="w-100">
                <div style="margin-top: 10px">
                    <p class="text-white d-flex align-items-center" style="gap: 5px"> <span
                            class="h3 mb-0">&#169;</span>
                        <span>Copyright 2021. All Right Reserved By PT. Roatex Indonesia Toll System</span>
                    </p>
                </div>
            </div>
        </div>
    </section>

    <script src="{{ asset('assets/libs/jquery/dist/jquery-3.6.3.min.js') }}"></script>
    <script src="{{ asset('dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('animation/js/wow.min.js') }}"></script>
    <script src="{{ asset('animation/js/f77c1b398bac9f249f3ffcaf76d290e8-1670674314.js') }}"></script>
</body>

</html>
