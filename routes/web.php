<?php

use App\Http\Controllers\Admin\CareerController as AdminCareerController;
use App\Http\Controllers\Admin\Content\AboutController;
use App\Http\Controllers\Admin\Content\HomeController;
use App\Http\Controllers\Admin\Content\SolutionController as ContentSolutionController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Home\HomepageController;
use App\Http\Controllers\Home\TechnologyController;
use App\Http\Controllers\Home\CareerController;
use App\Http\Controllers\Home\SolutionController;
use App\Http\Controllers\Home\NewsController;
use App\Http\Controllers\Home\AboutUsController;
use App\Http\Controllers\Home\ServicesController;

use App\Http\Controllers\Admin\LoginController;
use App\Http\Controllers\Admin\NewsController as AdminNewsController;
use App\Http\Controllers\Admin\UserController;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

Route::get('/', [HomepageController::class, 'index'])->name('homepage');
Route::get('/lang/id', function () {
    Session::put('locale', 'id');
    App::setLocale('id');
    return back();
})->name('lang-id');

Route::get('/lang/en', function () {
    Session::put('locale', 'en');
    return back();
})->name('lang-en');

Route::get('/home/{id}', [HomepageController::class, 'show'])->name('homepage-show');
Route::post('/login', [LoginController::class, 'login'])->name('login-attempt');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');

Route::get('/technology', [TechnologyController::class, 'index'])->name('tech-index');

Route::get('/aboutus', [AboutUsController::class, 'index'])->name('aboutus-index');

Route::get('/career', [CareerController::class, 'index'])->name('career-index');
Route::get('/career/{id}', [CareerController::class, 'show'])->name('career-show');

Route::get('/news', [NewsController::class, 'index'])->name('news-index');
Route::get('/news/{id}', [NewsController::class, 'show'])->name('news-show');

Route::get('/solution', [SolutionController::class, 'index'])->name('solution-index');

Route::get('/services', [ServicesController::class, 'index'])->name('services-index');

Route::get('/login', [LoginController::class, 'index'])->name('login-index');

Route::get('/dashboard', function () {
    return view('layouts.dashboard');
});


//admin
Route::prefix('/admin')->name('admin.')->middleware(['isUser'])->group(function () {
    Route::get(
        '/dashboard',
        function () {
            return view('pages.admin.dashboard');
        }
    )->name('dashboard');

    Route::prefix('/users')->name('users.')->group(
        function () {
            Route::get('/', [UserController::class, 'index'])->name('index');
            Route::post('/store', [UserController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [UserController::class, 'edit'])->name('edit');
            Route::post('/update', [UserController::class, 'update'])->name('update');
            Route::post('/action', [UserController::class, 'action'])->name('action');
        }
    );

    Route::prefix('/careers')->name('careers.')->group(
        function () {
            Route::get('/', [AdminCareerController::class, 'index'])->name('index');
            Route::post('/store', [AdminCareerController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [AdminCareerController::class, 'edit'])->name('edit');
            Route::post('/update', [AdminCareerController::class, 'update'])->name('update');
            Route::post('/action', [AdminCareerController::class, 'action'])->name('action');
        }
    );

    Route::prefix('/news')->name('news.')->group(
        function () {
            Route::get('/', [AdminNewsController::class, 'index'])->name('index');
            Route::post('/store', [AdminNewsController::class, 'store'])->name('store');
            Route::get('/edit/{id}', [AdminNewsController::class, 'edit'])->name('edit');
            Route::post('/update', [AdminNewsController::class, 'update'])->name('update');
            Route::post('/action', [AdminNewsController::class, 'action'])->name('action');
        }
    );

    Route::prefix('/content')->name('content.')->group(
        function () {
            Route::prefix('/home')->name('home.')->group(
                function () {
                    Route::get('/', [HomeController::class, 'index'])->name('index');
                    Route::get('/edit/{id}', [HomeController::class, 'edit'])->name('edit');
                    Route::post('/update', [HomeController::class, 'update'])->name('update');
                }
            );
            Route::prefix('/about-us')->name('about.')->group(
                function () {
                    Route::get('/', [AboutController::class, 'index'])->name('index');
                    Route::get('/edit/{id}', [AboutController::class, 'edit'])->name('edit');
                    Route::post('/update', [AboutController::class, 'update'])->name('update');
                }
            );
            Route::prefix('/solution')->name('solution.')->group(
                function () {
                    Route::get('/', [ContentSolutionController::class, 'index'])->name('index');
                    Route::get('/edit/{id}', [ContentSolutionController::class, 'edit'])->name('edit');
                    Route::post('/update', [ContentSolutionController::class, 'update'])->name('update');
                }
            );
        }
    );
});
