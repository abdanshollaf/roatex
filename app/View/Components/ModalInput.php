<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModalInput extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    public $form;
    public $title;
    public $formID;
    public $class;
    public function __construct($form = null, $title = null, $formID = null, $class = null)
    {
        $this->form = $form;
        $this->title = $title;
        $this->formID = $formID;
        $this->class = $class;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal-input');
    }
}
