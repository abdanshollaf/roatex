<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Content;
use App\Models\News;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class HomepageController extends Controller
{
    public function index()
    {
        if (Session::get('locale') == "") {
            Session::put('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        $data = Content::where('content_type', 'home')->orderBy('id', 'ASC')->get();
        $aboutus = Content::where('content_type', 'about-us')->orderBy('id', 'ASC')->get();
        $news = News::orderBy('id', 'desc')->limit(3)->get();
        return view('pages.homepage', compact('data', 'news', 'aboutus'));
    }

    public function show($id)
    {
        if (Session::get('locale') == "") {
            Session::puh('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        $data = Content::find($id);
        return view('pages.home-detail', compact('data'));
    }
}
