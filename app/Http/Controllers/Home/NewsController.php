<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class NewsController extends Controller
{
    public function index(Request $request)
    {
        if (Session::get('locale') == "") {
            Session::puh('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        $search = "";
        if ($request->input('search')) {
            $search = $request->input('search');
            $dataAll = News::where('is_deleted', '0')->where('title', 'like', '%' . $search . '%')->orWhere('desc', 'like', '%' . $search . '%')->paginate(5);
            $dataLimit = News::where('is_deleted', '0')->orderBy('id', 'desc')->limit(4)->get();
            return view('pages.news', compact('dataAll', 'dataLimit', 'search'));
        }
        $dataAll = News::where('is_deleted', '0')->paginate(5);
        $dataLimit = News::where('is_deleted', '0')->orderBy('id', 'desc')->limit(4)->get();
        return view('pages.news', compact('dataAll', 'dataLimit', 'search'));
    }

    public function show($id)
    {
        if (Session::get('locale') == "") {
            Session::put('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        $data = News::find($id);
        return view('pages.news-detail', compact('data'));
    }
}
