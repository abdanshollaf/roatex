<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class AboutUsController extends Controller
{
    public function index()
    {
        if (Session::get('locale') == "") {
            Session::put('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        $data = Content::where('content_type', 'about-us')->orderBy('id', 'ASC')->get();
        return view('pages.aboutus', compact('data'));
    }
}
