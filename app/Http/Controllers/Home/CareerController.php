<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Career;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class CareerController extends Controller
{
    public function index()
    {
        if (Session::get('locale') == "") {
            Session::put('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        $data = Career::where('is_deleted', '0')->orderBy('department', 'asc')->orderBy('created_at', 'desc')->get();
        return view('pages.career', compact('data'));
    }
    public function show($id)
    {
        if (Session::get('locale') == "") {
            Session::puh('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        $data = Career::find($id);
        return view('pages.career-detail', compact('data'));
    }
}
