<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class TechnologyController extends Controller
{
    public function index()
    {
        if (Session::get('locale') == "") {
            Session::put('locale', 'en');
        }
        App::setLocale(Session::get('locale'));
        return view('pages.technology');
    }
}
