<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index()
    {
        $data = User::all();
        return view('pages.admin.user.index', compact('data'));
    }
    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
                'password' => ['required', 'string', 'min:5'],
                'status' => ['required',],
            ]
        );

        if ($data->fails()) {
            $result = array('status' => 'failed', 'errors' => $data->errors());
        } else {
            User::create([
                'name' => $request->name,
                'email' => $request->email,
                'is_active' => $request->status,
                'user_inserted' => Auth::user()->name,
                'password' => Hash::make($request->password),
            ]);
            $result = array('status' => 'success');
        }
        echo json_encode($result);
    }

    public function edit(Request $request, $id)
    {
        $data = User::find($id);
        $data->password = '';
        return view('pages.admin.user.edit', compact('data'));
    }

    public function update(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'name' => ['required', 'string', 'max:255'],
                'email' => ['required', 'string', 'email', 'max:255'],
                'status' => ['required',],
            ]
        );

        if ($data->fails()) {
            $result = array('status' => 'failed', 'errors' => $data->errors());
        } else {
            $user = User::find($request->id);
            if ($user->email != $request->email) {
                $data = Validator::make(
                    $request->all(),
                    [
                        'email' => ['unique:users']
                    ]
                );
                if ($data->fails()) {
                    $result = array('status' => 'failed', 'errors' => $data->errors());
                }
            }
            if ($request->password != null) {
                $data = Validator::make(
                    $request->all(),
                    [
                        'password' => ['required', 'string', 'min:5'],
                    ]
                );
                if ($data->fails()) {
                    $result = array('status' => 'failed', 'errors' => $data->errors());
                }
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'is_active' => $request->status,
                    'password' => Hash::make($request->password),
                ]);
            } else {
                $user->update([
                    'name' => $request->name,
                    'email' => $request->email,
                    'is_active' => $request->status,
                ]);
                $result = array('status' => 'success');
            }
        }
        echo json_encode($result);
    }

    public function action(Request $request)
    {
        $data = User::find($request->id);
        $data->is_active = intval($request->action);
        $data->save();
        $result = array('status' => 'success', 'data' => $data);
        echo json_encode($result);
    }
}
