<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    public function index()
    {
        $data = News::orderBy('created_at', 'desc')->get();
        return view('pages.admin.news.index', compact('data'));
    }
    public function store(Request $request)
    {
        // dd($request);
        $data = Validator::make(
            $request->all(),
            [
                'article' => ['required', 'string'],
                'title' => ['required', 'string'],
                'title_ind' => ['required', 'string'],
                'desc' => ['required', 'string'],
                'desc_ind' => ['required', 'string'],
                'status' => ['required'],
                'image' => ['required', 'mimes:png,jpeg,jpg'],
            ]
        );

        if ($data->fails()) {
            $result = array('status' => 'failed', 'errors' => $data->errors());
        } else {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = 'News/' . $file->getClientOriginalName() . '_' . Str::random(5) . '.' . $extension;
            $file->move('News', $filename);
            News::create([
                'article' => $request->article,
                'title' => $request->title,
                'title_ind' => $request->title_ind,
                'desc' => $request->desc,
                'desc_ind' => $request->desc_ind,
                'image' => $filename,
                'user_inserted' => Auth::user()->name,
                'is_active' => $request->status,
            ]);
            $result = array('status' => 'success');
        }
        echo json_encode($result);
    }

    public function edit(Request $request, $id)
    {
        $data = News::find($id);
        return view('pages.admin.news.edit', compact('data'));
    }

    public function update(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'article' => ['required', 'string'],
                'title' => ['required', 'string'],
                'title_ind' => ['required', 'string'],
                'desc' => ['required', 'string'],
                'desc_ind' => ['required', 'string'],
                'status' => ['required'],
                'image' => ['mimes:png,jpeg,jpg'],
            ]
        );

        if ($data->fails()) {
            $result = array('status' => 'failed', 'errors' => $data->errors());
        } else {
            $news = News::find($request->id);
            if ($request->image != "") {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = 'News/' . $file->getClientOriginalName() . '_' . Str::random(5) . '.' . $extension;
                $file->move('News', $filename);
                $news->update([
                    'article' => $request->article,
                    'image' => $filename,
                    'title' => $request->title,
                    'title_ind' => $request->title_ind,
                    'desc' => $request->desc,
                    'desc_ind' => $request->desc_ind,
                    'is_deleted' => $request->status,
                ]);
            } else {
                $news->update([
                    'article' => $request->article,
                    'title' => $request->title,
                    'title_ind' => $request->title_ind,
                    'desc' => $request->desc,
                    'desc_ind' => $request->desc_ind,
                    'is_deleted' => $request->status,
                ]);
            }

            $result = array('status' => 'success');
        }
        echo json_encode($result);
    }

    public function action(Request $request)
    {
        $data = News::find($request->id);
        $data->is_deleted = intval($request->action);
        $data->save();
        $result = array('status' => 'success', 'data' => $data);
        echo json_encode($result);
    }
}
