<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Career;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CareerController extends Controller
{
    public function index()
    {
        $data = Career::orderBy('created_at', 'desc')->get();
        return view('pages.admin.career.index', compact('data'));
    }
    public function store(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'department' => ['required', 'string'],
                'department_ind' => ['required', 'string'],
                'title' => ['required', 'string'],
                'title_ind' => ['required', 'string'],
                'desc' => ['required', 'string'],
                'desc_ind' => ['required', 'string'],
                'status' => ['required'],
            ]
        );

        if ($data->fails()) {
            $result = array('status' => 'failed', 'errors' => $data->errors());
        } else {
            Career::create([
                'department' => $request->department,
                'department_ind' => $request->department_ind,
                'title' => $request->title,
                'title_ind' => $request->title_ind,
                'desc' => $request->desc,
                'desc_ind' => $request->desc_ind,
                'user_inserted' => Auth::user()->name,
                'is_active' => $request->status,
            ]);
            $result = array('status' => 'success');
        }
        echo json_encode($result);
    }

    public function edit(Request $request, $id)
    {
        $data = Career::find($id);
        return view('pages.admin.career.edit', compact('data'));
    }

    public function update(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'department' => ['required', 'string'],
                'department_ind' => ['required', 'string'],
                'title' => ['required', 'string'],
                'title_ind' => ['required', 'string'],
                'desc' => ['required', 'string'],
                'desc_ind' => ['required', 'string'],
                'status' => ['required'],
            ]
        );

        if ($data->fails()) {
            $result = array('status' => 'failed', 'errors' => $data->errors());
        } else {
            $career = Career::find($request->id);
            $career->update([
                'department' => $request->department,
                'department_ind' => $request->department_ind,
                'title' => $request->title,
                'title_ind' => $request->title_ind,
                'desc' => $request->desc,
                'desc_ind' => $request->desc_ind,
                'is_deleted' => $request->status,
            ]);
            $result = array('status' => 'success');
        }
        echo json_encode($result);
    }

    public function action(Request $request)
    {
        $data = Career::find($request->id);
        $data->is_deleted = intval($request->action);
        $data->save();
        $result = array('status' => 'success', 'data' => $data);
        echo json_encode($result);
    }
}
