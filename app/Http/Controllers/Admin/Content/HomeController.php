<?php

namespace App\Http\Controllers\Admin\Content;

use App\Http\Controllers\Controller;
use App\Models\Content;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class HomeController extends Controller
{
    public function index()
    {
        $data = Content::where('content_type', 'home')->orderBy('id')->get();
        return view('pages.admin.content.home.index', compact('data'));
    }

    public function edit($id)
    {
        $data = Content::find($id);
        return view('pages.admin.content.home.edit', compact('data'));
    }

    public function update(Request $request)
    {
        $data = Validator::make(
            $request->all(),
            [
                'title' => ['required', 'string'],
                'title_ind' => ['required', 'string'],
                'desc' => ['required', 'string'],
                'desc_ind' => ['required', 'string'],
                'image' => ['mimes:png,jpeg,jpg'],
                'video' => ['mimes:mp4,m4v,mov'],
            ]
        );

        if ($data->fails()) {
            $result = array('status' => 'failed', 'errors' => $data->errors());
        } else {
            $content = Content::find($request->id);
            if ($request->image != "") {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = 'Content/' . $file->getClientOriginalName() . '_' . Str::random(5) . '.' . $extension;
                $file->move('Content', $filename);
                $content->update([
                    'image' => $filename,
                    'title' => $request->title,
                    'title_ind' => $request->title_ind,
                    'desc' => $request->desc,
                    'desc_ind' => $request->desc_ind,
                ]);
            } else if ($request->video != "") {
                $file = $request->file('video');
                $extension = $file->getClientOriginalExtension();
                $filename = 'Content/' . $file->getClientOriginalName() . '_' . Str::random(5) . '.' . $extension;
                $file->move('Content', $filename);
                $content->update([
                    'video' => $filename,
                    'title' => $request->title,
                    'title_ind' => $request->title_ind,
                    'desc' => $request->desc,
                    'desc_ind' => $request->desc_ind,
                ]);
            } else {
                $content->update([
                    'title' => $request->title,
                    'title_ind' => $request->title_ind,
                    'desc' => $request->desc,
                    'desc_ind' => $request->desc_ind,
                ]);
            }

            $result = array('status' => 'success', 'data' => $content);
        }
        echo json_encode($result);
    }
}
