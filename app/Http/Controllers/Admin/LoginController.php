<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    public function index()
    {
        return view('layouts.login1');
    }

    public function login(Request $request)
    {
        $input = $request->all();

        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if (auth()->attempt(array('email' => $input['email'], 'password' => $input['password']))) {
            $user = User::where('email', $input['email'])->where('is_active', 1)->get();
            if (count($user) > 0) {
                $request->session()->regenerate();
                return to_route('admin.users.index');
            }
            return redirect()->route('login-index')
                ->with('error', 'Email-Address And Password Are Wrong.');
        } else {
            return redirect()->route('login-index')
                ->with('error', 'Email-Address And Password Are Wrong.');
        }
    }

    public function logout()
    {
        Session::flush();
        Auth::logout();
        return redirect('/login');
    }
}
