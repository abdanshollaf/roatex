<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    use HasFactory;

    protected $table = 'content';

    protected $primaryKey = 'id';

    protected $fillable = [
        'title',
        'desc',
        'desc2',
        'title_ind',
        'desc_ind',
        'desc_ind2',
        'image',
        'video',
    ];
}
