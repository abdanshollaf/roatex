<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Career extends Model
{
    use HasFactory;

    protected $table = 'career';

    protected $primaryKey = 'id';

    protected $fillable = [
        'department',
        'title',
        'desc',
        'department_ind',
        'title_ind',
        'desc_ind',
        'is_deleted',
        'user'
    ];
}
