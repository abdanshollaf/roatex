const protocol = location.origin;
const host = window.location.hostname;
const path = window.location.pathname;

$(document).ready(function () {
    $(document).on("click", "#submit-user", function (eve) {
        eve.preventDefault();
        $(".error-text").text("");
        var name = $("#name").val();
        var password = $("#password").val();
        var email = $("#email").val();
        var status = $("#status").val();
        var action = $("#form-user").attr("action");
        if (name == "") {
            $("#error-name").removeClass("hide");
            $("#error-name").text("Silahkan Input Nama!");
        } else if (email == "") {
            $("#error-email").removeClass("hide");
            $("#error-email").text("Silahkan Input Email!");
        } else if (password == "" && action == "store") {
            $("#error-password").removeClass("hide");
            $("#error-password").text("Silahkan Input Password!");
        } else if (status == "") {
            $("#error-status").removeClass("hide");
            $("#error-status").text("Silahkan Input Status!");
        } else {
            var datatosend = $("#form-user").serialize();
            if (action == "store") {
                base_url = protocol + path + "/" + action;
            } else {
                base_url = protocol + "/admin/users/update";
            }
            $.ajax(base_url, {
                dataType: "json",
                type: "POST",
                data: datatosend,
                beforeSend: function (data) {
                    Swal.fire({
                        title: "Please Wait!",
                        timerProgressBar: true,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            Swal.showLoading();
                        },
                    });
                },
                success: function (data) {
                    Swal.close();
                    if (data.status == "success") {
                        var message_action = "";
                        if (action == "store") {
                            message_action = "ditambahkan";
                            Swal.fire({
                                title: "Success",
                                text: "User Data sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.history.pushState(null, null, path);
                                    window.location.reload(true);
                                    $("#myModal").modal("hide");
                                }
                            });
                        } else {
                            message_action = "diperbaharui";
                            Swal.fire({
                                title: "Success",
                                text: "User Data sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.location = protocol + "/admin/users";
                                    $("#myModal").modal("hide");
                                }
                            });
                        }
                    } else {
                        Swal.fire({
                            title: "Warning!",
                            text: "Data Tidak Tersimpan!",
                            icon: "error",
                        });
                        $.each(data.errors, function (index, element) {
                            $("#error-" + index).text("");
                            $("#error-" + index).removeClass("hide");
                            $("#error-" + index).text(element);
                        });
                    }
                },
                error: function (data) {
                    Swal.fire({
                        title: "Error!",
                        text: "Data Tidak Tersimpan!",
                        icon: "error",
                    });
                },
            });
        }
    });

    $(document).on("click", "#submit-career", function (eve) {
        eve.preventDefault();
        $(".error-text").text("");
        var department = $("#department").val();
        var title = $("#title").val();
        var desc = $("#desc").val();
        var status = $("#status").val();
        var action = $("#form-career").attr("action");
        if (department == "") {
            $("#error-department").removeClass("hide");
            $("#error-department").text("Silahkan Input Departemen / Divisi!");
        } else if (title == "") {
            $("#error-title").removeClass("hide");
            $("#error-title").text("Silahkan Input Posisi!");
        } else if (desc == "") {
            $("#error-desc").removeClass("hide");
            $("#error-desc").text("Silahkan Input Deskripsi!");
        } else if (status == "") {
            $("#error-status").removeClass("hide");
            $("#error-status").text("Silahkan Input Status!");
        } else {
            var datatosend = $("#form-career").serialize();
            if (action == "store") {
                base_url = protocol + path + "/" + action;
            } else {
                base_url = protocol + "/admin/careers/update";
            }
            $.ajax(base_url, {
                dataType: "json",
                type: "POST",
                data: datatosend,
                beforeSend: function (data) {
                    Swal.fire({
                        title: "Please Wait!",
                        timerProgressBar: true,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            Swal.showLoading();
                        },
                    });
                },
                success: function (data) {
                    Swal.close();
                    if (data.status == "success") {
                        var message_action = "";
                        if (action == "store") {
                            message_action = "ditambahkan";
                            Swal.fire({
                                title: "Success",
                                text: "Data Karir sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.history.pushState(null, null, path);
                                    window.location.reload(true);
                                    $("#myModal").modal("hide");
                                }
                            });
                        } else {
                            message_action = "diperbaharui";
                            Swal.fire({
                                title: "Success",
                                text: "Data Karir sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.location =
                                        protocol + "/admin/careers";
                                    $("#myModal").modal("hide");
                                }
                            });
                        }
                    } else {
                        Swal.fire({
                            title: "Warning!",
                            text: "Data Tidak Tersimpan!",
                            icon: "error",
                        });
                        $.each(data.errors, function (index, element) {
                            $("#error-" + index).text("");
                            $("#error-" + index).removeClass("hide");
                            $("#error-" + index).text(element);
                        });
                    }
                },
                error: function (data) {
                    Swal.fire({
                        title: "Error!",
                        text: "Data Tidak Tersimpan!",
                        icon: "error",
                    });
                },
            });
        }
    });

    $(document).on("click", "#submit-news", function (eve) {
        eve.preventDefault();
        $(".error-text").text("");
        var article = $("#article").val();
        var title = $("#title").val();
        var desc = $("#desc").val();
        var status = $("#status").val();
        var image = $("#image").val();
        var action = $("#form-news").attr("action");
        if (article == "") {
            $("#error-article").removeClass("hide");
            $("#error-article").text("Silahkan Input Link Berita!");
        } else if (title == "") {
            $("#error-title").removeClass("hide");
            $("#error-title").text("Silahkan Input Judul Berita!");
        } else if (desc == "") {
            $("#error-desc").removeClass("hide");
            $("#error-desc").text("Silahkan Input Isi Berita!");
        } else if (image == "" && action == "store") {
            $("#error-image").removeClass("hide");
            $("#error-image").text("Silahkan Input Gambar!");
        } else if (status == "") {
            $("#error-status").removeClass("hide");
            $("#error-status").text("Silahkan Input Status!");
        } else {
            var form = $("#form-news")[0];
            var datatosend = new FormData(form);
            if (action == "store") {
                base_url = protocol + path + "/" + action;
            } else {
                base_url = protocol + "/admin/news/update";
            }
            $.ajax(base_url, {
                dataType: "json",
                type: "POST",
                data: datatosend,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function (data) {
                    Swal.fire({
                        title: "Please Wait!",
                        timerProgressBar: true,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            Swal.showLoading();
                        },
                    });
                },
                success: function (data) {
                    Swal.close();
                    if (data.status == "success") {
                        var message_action = "";
                        if (action == "store") {
                            message_action = "ditambahkan";
                            Swal.fire({
                                title: "Success",
                                text: "Data Berita sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.history.pushState(null, null, path);
                                    window.location.reload(true);
                                    $("#myModal").modal("hide");
                                }
                            });
                        } else {
                            message_action = "diperbaharui";
                            Swal.fire({
                                title: "Success",
                                text: "Data Berita sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.location = protocol + "/admin/news";
                                    $("#myModal").modal("hide");
                                }
                            });
                        }
                    } else {
                        Swal.fire({
                            title: "Warning!",
                            text: "Data Tidak Tersimpan!",
                            icon: "error",
                        });
                        $.each(data.errors, function (index, element) {
                            $("#error-" + index).text("");
                            $("#error-" + index).removeClass("hide");
                            $("#error-" + index).text(element);
                        });
                    }
                },
                error: function (data) {
                    Swal.fire({
                        title: "Error!",
                        text: "Data Tidak Tersimpan!",
                        icon: "error",
                    });
                },
            });
        }
    });

    $(document).on("click", "#submit-content", function (eve) {
        eve.preventDefault();
        $(".error-text").text("");
        var title = $("#title").val();
        var desc = $("#desc").val();
        var content_type = $("#content_type").val();
        var action = $("#form-content").attr("action");
        if (title == "") {
            $("#error-title").removeClass("hide");
            $("#error-title").text("Silahkan Input Judul!");
        } else if (desc == "") {
            $("#error-desc").removeClass("hide");
            $("#error-desc").text("Silahkan Input Deskripsi!");
        } else {
            var form = $("#form-content")[0];
            var datatosend = new FormData(form);
            base_url = protocol + "/admin/content/" + content_type + "/update";
            $.ajax(base_url, {
                dataType: "json",
                type: "POST",
                data: datatosend,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function (data) {
                    Swal.fire({
                        title: "Please Wait!",
                        timerProgressBar: true,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            Swal.showLoading();
                        },
                    });
                },
                success: function (data) {
                    Swal.close();
                    if (data.status == "success") {
                        var message_action = "";
                        if (action == "store") {
                            message_action = "ditambahkan";
                            Swal.fire({
                                title: "Success",
                                text: "Data Content sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.history.pushState(null, null, path);
                                    window.location.reload(true);
                                    $("#myModal").modal("hide");
                                }
                            });
                        } else {
                            message_action = "diperbaharui";
                            Swal.fire({
                                title: "Success",
                                text: "Data Content sudah " + message_action,
                                icon: "success",
                                allowOutsideClick: false,
                            }).then((result) => {
                                if (result.value) {
                                    window.location = protocol + "/admin/content/" + content_type;
                                    $("#myModal").modal("hide");
                                }
                            });
                        }
                    } else {
                        Swal.fire({
                            title: "Warning!",
                            text: "Data Tidak Tersimpan!",
                            icon: "error",
                        });
                        $.each(data.errors, function (index, element) {
                            $("#error-" + index).text("");
                            $("#error-" + index).removeClass("hide");
                            $("#error-" + index).text(element);
                        });
                    }
                },
                error: function (data) {
                    Swal.fire({
                        title: "Error!",
                        text: "Data Tidak Tersimpan!",
                        icon: "error",
                    });
                },
            });
        }
    });
});

$("#mymodal").on("hidden.bs.modal", function () {
    $(".error-text").text("");
    $("#mymodal form")[0].reset();
});

function postJSON(url, data) {
    return JSON.parse(
        $.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: "json",
            global: false,
            async: false,
            beforeSend: function (msg) {
                Swal.fire({
                    title: "Please Wait!",
                    timerProgressBar: true,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    didOpen: () => {
                        Swal.showLoading();
                    },
                });
            },
            success: function (msg) {
                swal.close();
            },
        }).responseText
    );
}

function postMultipart(url, data) {
    return JSON.parse(
        $.ajax({
            type: "POST",
            url: url,
            data: new FormData(data),
            global: false,
            async: false,
            beforeSend: function (msg) {
                Swal.fire({
                    title: "Please Wait!",
                    timerProgressBar: true,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    didOpen: () => {
                        Swal.showLoading();
                    },
                });
            },
            success: function (msg) {
                swal.close();
            },
        }).responseText
    );
}

function getJSON(url, data) {
    return JSON.parse(
        $.ajax({
            type: "GET",
            url: url,
            data: data,
            dataType: "json",
            global: false,
            async: false,
            beforeSend: function (msg) {
                Swal.fire({
                    title: "Please Wait!",
                    timerProgressBar: true,
                    showConfirmButton: false,
                    allowOutsideClick: false,
                    didOpen: () => {
                        Swal.showLoading();
                    },
                });
            },
            success: function (msg) {
                swal.close();
            },
        }).responseText
    );
}

function actionData(i) {
    var id = $(i).data("id");
    var action = $(i).data("action");
    var aksi = $(i).data("aksi");
    var token = $('meta[name="_token"]').attr("content");

    Swal.fire({
        title: "Apa anda yakin ?",
        text: "Kamu akan " + aksi,
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes!",
    }).then((result) => {
        if (result.isConfirmed) {
            var datatosend = {};
            datatosend.id = id;
            datatosend.action = action;
            datatosend._token = token;
            $.ajax(protocol + path + "/action", {
                dataType: "json",
                type: "POST",
                data: datatosend,
                beforeSend: function (data) {
                    Swal.fire({
                        title: "Please Wait!",
                        timerProgressBar: true,
                        showConfirmButton: false,
                        allowOutsideClick: false,
                        didOpen: () => {
                            Swal.showLoading();
                        },
                    });
                },
                success: function (data) {
                    swal.close();
                    if (data.status == "success") {
                        Swal.fire({
                            title: "Success",
                            text: "Berhasil! Sudah di" + aksi,
                            icon: "success",
                        }).then((result) => {
                            if (result.value) {
                                window.history.pushState(null, null, path);
                                window.location.reload(true);
                            }
                        });
                    } else {
                        Swal.fire({
                            title: "Failed",
                            text: "Gagal " + aksi,
                            icon: "error",
                        });
                    }
                },
                error: function (data) {
                    Swal.fire({
                        title: "Failed",
                        text: "Gagal " + aksi,
                        icon: "error",
                    });
                },
            });
        }
    });
}
